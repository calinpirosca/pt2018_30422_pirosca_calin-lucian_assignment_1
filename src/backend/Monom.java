package backend;

import com.github.kiprobinson.bigfraction.BigFraction;

public class Monom {

	private double coef; // an integer value will be considered as it is, a real value will be
							// transformed to a fraction when displayed
	private String variable; // default shall be considered 'x'
	private int power; // a monom with negative power will be considered 0

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public Monom() { // an unitialized monom will have coef = 1, power = 1, variable = 'x'
		this(0, "", 0);
	}

	public Monom(double coef, String variable, int power) {
		this.coef = coef;
		this.power = power;
		this.variable = variable;
	}

	public Monom(Monom other) { // copy constructor used for deep copying
		this.coef = other.coef;
		this.variable = other.variable;
		this.power = other.power;
	}

	public Monom setMonom(String input) { // Setting and validating data for an input monom
		// Assume that the input will have no sign, as it is splitted already.
		this.power = 0;
		this.coef = 1;
		this.variable = ""; // reinitialize the monom just in case there were some old values and it is
							// being reused
		String[] cp;

		if (input != null) {
			cp = input.split("(?<=\\^)|(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
			cp = Functions.MonomSplit(cp); // used to split monoms 
			if (cp.length == 3) {// 0 coef,1 var, 2 power
				try {
					this.coef = Double.valueOf(cp[0]);
					if (cp[1].matches("([a-z]+\\^)$")) {
						this.variable = cp[1].substring(0, cp[1].length() - 1);
					} else { // the variable doesn't have a valid input.
						return null;
					}
					this.power = Integer.parseInt(cp[2]);
				} catch (NumberFormatException e) {
					return null;
				}
			} else if (cp.length == 2) { // 0 coef/var, 1 var/power
				try {
					this.coef = Double.parseDouble(cp[0]);
					if (cp[1].matches("([a-zA-Z]+)")) {
						this.variable = cp[1];
						this.power = 1;
					} else {
						return null; // the var has an invalid format
					}
				} catch (NumberFormatException e) {// invalid coef, or it's 0 var, 1 power
					if (cp[0].matches("(([a-zA-Z]+\\^)$)|([a-zA-Z]+)") && input.contains("^")) {
						this.variable = cp[0].substring(0, cp[0].length() - 1);// remove ^
					} else {
						return null; // the var has an invalid format
					}
					try {
						this.power = Integer.parseInt(cp[1]);
					} catch (NumberFormatException e1) { // the power has an invalid format
						return null;
					}
				}
			} else if (cp.length == 1) {// 0 coef or var
				try {
					this.coef = Double.parseDouble(cp[0]);
				} catch (NumberFormatException e) { // data doesn't have coef format
					if (cp[0].matches("([a-zA-Z]+)")) {
						this.variable = cp[0];
						this.power = 1;
					} else {
						return null; // data doesn't have var format
					}
				}
			} else {
				return null;
			}
			return new Monom(this.coef, this.variable, this.power);
		} else {
			return null;
		}
	}

	public String getMonom() {
		String monom = "";

		if (this.coef == 0) {
			return "0";
		} else if (this.coef > 0) {
			monom = "+";
		} else if (this.coef == -1) {
			monom = "-";
			if (this.power == 0) {
				monom = String.valueOf((int) this.coef);
			}
		}
		if (this.coef != 1 && this.coef != -1) {
			if (Math.floor(this.coef) != this.coef) { // it's a fraction
				monom = monom + BigFraction.valueOf(Functions.getRepeatingDecimals(this.coef)).toString();//Functions.toFraction(this.coef);
			} else {
				monom = monom + (int) this.coef;
			}
		}
		if (this.power == 0 && this.coef == 1) {
			monom = monom + (int) this.coef;
		} else if (power == 1) {
			monom = monom + this.variable;
		} else if (power > 1) {
			monom = monom + this.variable + "^" + this.power;
		}

		return monom;
	}

}
