package backend;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynom {

	private List<Monom> members;
	@SuppressWarnings("serial")
	final static List<Pattern> patterns = new ArrayList<Pattern>() {
		{
			add(Pattern.compile("(?<=(\\+|\\-|\\A|\\s))([0-9/]+[a-zA-Z]+)(\\^)(\\+|\\-)?([0-9]+)(?=\\+|\\-|\\s|\\z)")); // coef+var+power
			add(Pattern.compile("(?<=(\\+|\\-|\\A|\\s))([a-zA-Z]+)(\\^)(\\+|\\-)?([0-9]+)(?=\\+|\\-|\\s|\\z)")); //var + power
			add(Pattern.compile("(?<=(\\+|\\-|\\A|\\s))([0-9/]+[a-zA-Z]+)(?=\\+|\\-|\\s|\\z)")); // coef + var
			add(Pattern.compile("(?<=(([^\\^]*\\+|\\-)|\\A|\\s))(([0-9/]+)|([a-zA-Z]+))(?=\\+|\\-|\\s|\\z)")); /// var or
																												/// coef and avoid ^
		}
	};// patterns must be matched in this particular order

	public Polynom(String poly) {
		members = new ArrayList<Monom>();
		this.setPoly(poly);
	}

	public Polynom(Polynom other) { // deep copy
		this.members = new ArrayList<Monom>();
		for (Monom x : other.getMembers()) {
			members.add(new Monom(x));
		}
	}

	public List<Monom> getMembers() {
		return members;
	}

	public void setMembers(List<Monom> members) {
		this.members = members;
	}

	public int getHDegree() { // return highest degree
		if(members.size() > 0) {
			return members.get(0).getPower();
		}
		return -1;
	}

	public double getCoefHDegree() { // return the coefficient of highest degree
		if(members.size() > 0) {
			return members.get(0).getCoef();
		}
		return -1;
	}

	public Polynom getLPoly() { // return latest monom as a single polynom
		Polynom lPoly = new Polynom("");
		lPoly.addMonom(members.get(members.size() - 1));
		return lPoly;
	}

	public boolean addMonom(Monom member) { // Adaug monoamele in functie de grad
		// Since the user cannot add a monom by himself, only a polynom, we'll consider
		// that the data input is valid
		if(member == null) {
			return false;
		}
		Monom next, prev;
		ListIterator<Monom> prevIt = members.listIterator();
		ListIterator<Monom> nextIt = members.listIterator();
		while (nextIt.hasNext()) {
			next = nextIt.next();
			if (next.getPower() < member.getPower()) { // highest degree monom
				nextIt.previous();
				nextIt.add(member);
				return true;
			} else if (next.getPower() == member.getPower()) { // equal degree monom
				next.setCoef(next.getCoef() + member.getCoef());
				return true;
			} else {
				if (prevIt.hasPrevious()) {
					prev = prevIt.previous();
					if (prev.getPower() > member.getPower() && next.getPower() < member.getPower()) { // add in between
						prevIt.add(member);
						return true;
					}
				} else {
					prevIt.next();
				}
			}
		}
		nextIt.add(member); // it means that it is the lowest degree monom, added at the end
		return true;
	}

	public String getPoly() {
		String poly = "";

		for (Monom m1 : members) {
			if (m1.getCoef() != 0) {
				poly = poly.concat(m1.getMonom()); // lipesc monoamele unu dupa altul. Sunt in ordine.
			}
		}

		if (poly.length() > 0) {
			if (poly.charAt(0) == '+') {
				poly = poly.substring(1); // daca primu caracter e pozitiv sterg + din fata
			}
		} else {
			poly = "0";// nu are niciun monom, inseamna ca ii 0.
		}
		return poly;
	}

	public void setPoly(String input) {
		members = new ArrayList<Monom>();
		Monom temp;

		for (Pattern pat : patterns) {
			Matcher matcher = pat.matcher(input);
			while (matcher.find()) {
				temp = new Monom();
				int k = 0;
				if(matcher.group(0).equals("0")) {
					temp = temp.setMonom(matcher.group(1).substring(1) + matcher.group(4));
					if(temp!= null) {
						temp.setCoef(temp.getCoef() * -1);
					}
				}else {
					temp = temp.setMonom(matcher.group(0));
					if(matcher.group(1).equals("-") && temp!= null) {
						temp.setCoef(temp.getCoef() * -1);
					}
				}
				
				this.addMonom(temp);
				if (matcher.start() > 0) {
					k  = 1;
				}
				StringBuffer text = new StringBuffer(input);
				text.replace(matcher.start() - k, matcher.end(), ""); // remove the matched monom and the sign because
																		// the lookahead doesn't capture the sign
				input = text.toString();
				input = input.trim(); // remove any white spaces from beginning or end
				matcher = pat.matcher(input);
			}
		}
	}
}
