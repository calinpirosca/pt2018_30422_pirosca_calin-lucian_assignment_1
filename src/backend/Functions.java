package backend;

import java.util.ArrayList;
import java.util.List;

public class Functions {
	// Each time a function is called in here, because the parameters are by
	// reference and will be modified, for each function
	// I will clone the given polynoms so they can be reused

	public static String[] MonomSplit(String[] cp) {
		if (cp.length >= 3) {
			if (cp[1].equals("/")) {
				double temp = Math.round(Double.valueOf(cp[0]));
				double temp1 = Math.round(Double.valueOf(cp[2]));
				String dif = String.valueOf(temp / temp1);
				int length = dif.length() - dif.indexOf(".");
				String cpp = dif;
				switch (cp.length) {
				case 3:
					cp = new String[] { cpp };
					break;
				case 4:
					cp = new String[] { cpp, cp[3] };
					break;
				case 5:
					cp = new String[] { cpp, cp[3], cp[4] };
					break;
				default:
					break;
				}
			}
		}
		return cp;
	}

	public static String getRepeatingDecimals(double input) { // used to determine repeating decimals and return it in a
																// format specified below
		String output = String.valueOf(input);
		String intreg = String.valueOf(output.substring(0, output.indexOf(".")));
		output = output.substring(output.indexOf(".") + 1);
		int startAt = 0, length = 0;
		for (Character x : output.toCharArray()) { // parcurg caracter cu caracter
			String first = String.valueOf(output.charAt(startAt)); // obtin primu caracter curent
			String sub = output.substring(startAt + 1); // obtin siru de dupa primu caracter curent
			length = sub.indexOf(first) + 1; // aflu peste cate caractere se repeta primu caracter
			if (length == 0) {
				startAt++;
				continue;
			}
			String sub1 = output.substring(startAt, length + startAt); // obtin siru de la primu caracter pana la prima
																		// aparitie a sa din nou
			String sub2 = sub.substring(length - 1, length * 2 - 1); // obtin siru de la primu aparitie a sa pana peste
																		// length caractere
			if (sub1.equals(sub2) && sub1.length() > 0) {
				break;
			} else {
				startAt++;
			}
		}
		if (length != 0) {
			return intreg + "." + output.substring(0, startAt) + "(" + output.substring(startAt, startAt + length)
					+ ")"; // format X.X(X) ex 1.23(4) sau 0.(3)
		} else {
			return String.valueOf(input);
		}

	}

	public static Polynom clone(Polynom a) {
		Polynom clone = new Polynom(a);

		return clone;
	}
	// my own version of to fraction
	// Replaced with bigFraction because it can process repeating decimals too
	/*
	 * public static String toFraction(double input) { // 1.6 will be transformed to
	 * 16 and 10
	 * 
	 * boolean negative = false;
	 * 
	 * if(input < 0) { input = input * -1; negative = true; }
	 * 
	 * String number = String.valueOf(input); if(number.length() -
	 * number.indexOf(".") > 3) { number =
	 * number.substring(0,number.indexOf(".")+3); } int num =
	 * Integer.parseInt(number.substring(0, number.indexOf("."))); num = num *
	 * (int)Math.pow(10, number.length() - 1 - number.indexOf("."));
	 * 
	 * int den = Integer.parseInt(number.substring(number.indexOf(".") + 1));
	 * 
	 * if(negative == true) { return "-"+simplifyFraction(num+den,
	 * String.valueOf(number).length()-1-String.valueOf(number).indexOf("."));//
	 * String.valueOf(num+den).length() - 1); }else { return
	 * simplifyFraction(num+den,
	 * String.valueOf(number).length()-1-String.valueOf(number).indexOf("."));//
	 * String.valueOf(num+den).length() - 1); } }
	 * 
	 * public static String simplifyFraction(int num, int den) { if(den == 0) {
	 * den++; } den = (int)Math.pow(10, den);
	 * 
	 * int gcd = BigInteger.valueOf(num).gcd(BigInteger.valueOf(den)).intValue();
	 * 
	 * return String.valueOf(num/gcd) + "/" + String.valueOf(den/gcd); }
	 */

	public static Polynom add(Polynom a, Polynom b, boolean negative) {
		List<Monom> polyB;

		Polynom a1 = Functions.clone(a); // created copies and modified them
		if (negative == false) {
			Polynom b1 = Functions.clone(b);
			polyB = b1.getMembers();
		} else {
			polyB = b.getMembers(); // it was already copied once and all members negated and doesn't need a third
									// copy.
		}

		for (Monom B : polyB) {
			a1.addMonom(B);
		}

		return a1;
	}

	public static Polynom substract(Polynom a, Polynom b) { // a - b
		Polynom b1 = Functions.clone(b); // same as above

		List<Monom> members = b1.getMembers();

		for (Monom x : members) {
			x.setCoef(x.getCoef() * (-1));
		}

		return add(a, b1, true); // I use true so that I won't create another copy of b and lose the negated
									// values
	}

	public static Polynom multiply(Polynom a, Polynom b) {
		// multiplication with another polynom will also be considered as multiplication
		// with a scalar, that scalar being interpeted as a polynom
		Polynom c = new Polynom("");

		Polynom a1 = Functions.clone(a);
		Polynom b1 = Functions.clone(b);

		List<Monom> a1Members = a1.getMembers();
		List<Monom> b1Members = b1.getMembers();
		Monom temp;

		for (Monom x : a1Members) {
			for (Monom y : b1Members) {
				temp = new Monom(x.getCoef() * y.getCoef(), x.getVariable().length()>0?x.getVariable():y.getVariable(), x.getPower() + y.getPower());
				//if one has variable and the other doesn't, we'll take the variable
				c.addMonom(temp);
			}
		}

		return c;
	}
	
	public static Polynom remove0Coef(Polynom a) {//removes any monom that has coef 0
		Polynom c = new Polynom("");
		for(Monom aMonoms :a.getMembers()) {
			if(aMonoms.getCoef() != 0) {
				c.addMonom(aMonoms);
			}
		}
		return c;
	}

	public static List<Polynom> divide(Polynom a, Polynom b) {
		// Considering the fact that the division of coefficients is an integer
		// number,otherwise answer is 0
		Polynom a1 = Functions.clone(a);

		List<Polynom> result = new ArrayList<Polynom>(); // contains the quotient and the remainder
		Polynom qtn = new Polynom(""); // quotient
		Polynom rmn = Functions.clone(b); // remainder
		Monom last;

		// if the degree of A is not bigger or equal to the degree of B the division
		// cannot take place
		if (a1.getHDegree() < rmn.getHDegree()) {
			return result;
		} else {
			while (rmn.getHDegree() <= a1.getHDegree()) {
				last = new Monom(a1.getCoefHDegree() / rmn.getCoefHDegree(), "x", a1.getHDegree() - rmn.getHDegree());
				qtn.addMonom(last);

				a1 = Functions.substract(a1, Functions.multiply(qtn.getLPoly(), rmn));
				a1 = Functions.remove0Coef(a1); // sterg orice monom cu coeficient 0 ce ar fi putut rezulta in urma scaderii
			}
		}
		result.add(qtn);
		result.add(a1);
		return result;
	}

	public static Polynom derivation(Polynom a) {

		Polynom a1 = Functions.clone(a);
		List<Monom> a1Members = a1.getMembers();
		Monom freeterm = null;

		for (Monom x : a1Members) {
			if (x.getPower() == 0) { // memorize the freeterm so I can remove it instead of setting it to 0
				freeterm = x;
			} else {
				x.setCoef(x.getCoef() * x.getPower());
				x.setPower(x.getPower() - 1);
			}
		}
		if (freeterm != null) { // if a free term was found I'll remove it
			a1Members.remove(freeterm);
		}
		return a1;
	}

	public static Polynom integrate(Polynom a) {
		Polynom a1 = Functions.clone(a);
		List<Monom> a1Members = a1.getMembers();

		for (Monom x : a1Members) {
			x.setPower(x.getPower() + 1);
			if(x.getPower() == 1) {
				x.setVariable("x");
			}
			x.setCoef(x.getCoef() / x.getPower());
		}

		return a1;
	}
}
