package frontend.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import backend.Functions;
import backend.Polynom;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class Controller {

	@FXML
	private Label xLabel, qLabel, result, shortcutw, shortcuta, shortcutd, shortcutm, shortcuttitle, shortcutinfo,
			shortcuts;

	@FXML
	private JFXTextField aInput, bInput;

	@FXML
	private JFXButton add, substract, multiply, divide, intA, intB, derA, derB;

	private final KeyCombination altS = new KeyCodeCombination(KeyCode.S, KeyCombination.ALT_DOWN); // switch
	private final KeyCombination ctrlW = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN); // add
	private final KeyCombination ctrlS = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN); // substract
	private final KeyCombination ctrlM = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN); // multiply
	private final KeyCombination ctrlD = new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN); // divide
	private final KeyCombination ctrlE = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN); // exit

	private Polynom a, b, r, q;

	@FXML
	public void initialize() { // loads the buttons so that there is no delay when pressing the button for the first time
		add.setOnAction(e -> {
			arithmeticOperaitons(1);
		});
		substract.setOnAction(e -> {
			arithmeticOperaitons(2);
		});
		multiply.setOnAction(e -> {
			arithmeticOperaitons(3);
		});
		divide.setOnAction(e -> {
			arithmeticOperaitons(4);
		});
		intA.setOnAction(e -> {
			arithmeticOperaitons(5);
		});
		intB.setOnAction(e -> {
			arithmeticOperaitons(6);
		});
		derA.setOnAction(e -> {
			arithmeticOperaitons(7);
		});
		derB.setOnAction(e -> {
			arithmeticOperaitons(8);
		});
	}

	@FXML
	private void closeButton() { // method for a label to act as a close button
		Stage current = (Stage) xLabel.getScene().getWindow();
		current.close();
	}

	@FXML
	private void setA() { // set results as polynom a
		aInput.setText(result.getText());
		result.setText("");
	}

	@FXML
	private void switchPolyInput(KeyEvent e) { // cand apas enter, sa treaca de la fieldul unui polynom la altul fara ca
												// utilizatorul sa selecteze cu mouseul
		if (e.getCode() == KeyCode.ENTER) {
			if (aInput.isFocused()) {
				bInput.requestFocus();
			} else {
				aInput.requestFocus();
			}
		}
	}

	@FXML
	private void handleSwitch() { // redirect
		arithmeticOperaitons(0);
	}

	@FXML
	private void handleShortcuts(KeyEvent e) {
		if (altS.match(e)) { // switch the values of the 2 text fields
			arithmeticOperaitons(0);
		} else if (ctrlW.match(e)) {// add
			arithmeticOperaitons(1);
		} else if (ctrlS.match(e)) {// substract
			arithmeticOperaitons(2);
		} else if (ctrlM.match(e)) {// multiply
			arithmeticOperaitons(3);
		} else if (ctrlD.match(e)) {// divde
			arithmeticOperaitons(4);
		} else if (ctrlE.match(e)) {
			this.closeButton();
		}
	}

	private void arithmeticOperaitons(int option) {
		a = new Polynom(aInput.getText());
		b = new Polynom(bInput.getText());
		// if (a.getMembers().size() != 0 && b.getMembers().size() != 0) { // making
		// sure there is correct input for both of them
		switch (option) {
		case 0: // swtich
			String string = aInput.getText();
			aInput.setText(bInput.getText());
			bInput.setText(string);
			break;
		case 1: // add
			result.setText(Functions.add(a, b, false).getPoly());
			break;
		case 2: // substract
			result.setText(Functions.substract(a, b).getPoly());
			break;
		case 3: // multiply
			result.setText(Functions.multiply(a, b).getPoly());
			break;
		case 4: // divide
			q = Functions.divide(a, b).get(0);
			r = Functions.divide(a, b).get(1);
			result.setText("Quotient : " + q.getPoly() + " Remainder : " + r.getPoly());
			break;
		case 5: // integrate a
			result.setText(Functions.integrate(a).getPoly());
			break;
		case 6:
			result.setText(Functions.integrate(b).getPoly());
			break;
		case 7:
			result.setText(Functions.derivation(a).getPoly());
			break;
		case 8:
			result.setText(Functions.derivation(b).getPoly());
			break;
		default:
			break;
		}
		// }
	}

	private volatile boolean stopDisplay; // used for stopping the thread

	@FXML
	private void helpButton() {
		stopDisplay = false;
		Thread display = new Thread(new Runnable() {
			@Override
			public void run() {
				while (stopDisplay == false) {
					if (stopDisplay == true) {
						break;
					}
					Platform.runLater(() -> {
						shortcutinfo.setVisible(true);
					});
					try {
						Thread.sleep(700);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcuttitle.setVisible(true);
					});
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutw.setVisible(true);
					});
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcuts.setVisible(true);
					});
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutm.setVisible(true);
					});
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutd.setVisible(true);
					});
					try {
						Thread.sleep(7000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutd.setVisible(false);
					});
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutm.setVisible(false);
					});
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcuts.setVisible(false);
					});
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutw.setVisible(false);
					});
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcuttitle.setVisible(false);
					});
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						shortcutinfo.setVisible(false);
					});
					stopDisplay = true;
				}
			}
		});
		display.start();
	}
}
