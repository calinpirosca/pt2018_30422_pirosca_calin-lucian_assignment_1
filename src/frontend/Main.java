package frontend;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws IOException {
		primaryStage.setTitle("Polynomial Calculator");
		primaryStage.initStyle(StageStyle.UNDECORATED); //to remove minimize,maximize,close button
		Parent root = FXMLLoader.load(getClass().getResource("/frontend/view/startStage.fxml"));
		Scene scene = new Scene(root,710,385);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
