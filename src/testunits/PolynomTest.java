package testunits;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import backend.Polynom;

public class PolynomTest {

	final static String[] original = new String[] { "", "1", "+1", "-1", "x", "+x", "-x", "1x", "+1x", "-1x", "0x",
			"+0x", "-0x", "2x", "+2x", "-2x", "x^0", "x^1", "x^2", "1x^1", "2x^1", "2x^2", "+x^1", "-x^1", "+x^2",
			"-x^2", "+2x^1", "-2x^1", "-2x^2", "+2x^2", "+2x^2+2x^2", "+2x^2-2x^2", "-1+x+x^1+x^2+2x^3-2x^4-2x^5",
			"x^3+x^2-x-2x^0", "x^3+x^2-x^1-2", "x^3+x^2-x-2", "x^3+x^2-x^-2-2^2", "2/5x+3/5x^1", "2/2x", "2/1x", "2/4x",
			"abc", "123ad!@", "!@#x^2", "@b", "mn", "x^+2", "1.2x", "x^-3" };
	final static String[] expected = new String[] { "0", "1", "1", "-1", "x", "x", "-x", "x", "x", "-x", "0", "0", "0",
			"2x", "2x", "-2x", "1", "x", "x^2", "x", "2x", "2x^2", "x", "-x", "x^2", "-x^2", "2x", "-2x", "-2x^2",
			"2x^2", "4x^2", "0", "-2x^5-2x^4+2x^3+x^2+2x-1", "x^3+x^2-x-2", "x^3+x^2-x-2", "x^3+x^2-x-2", "x^3+x^2",
			"x", "x", "2x", "1/2x", "abc", "0", "0", "0", "mn", "0", "0", "0" };
	final static List<String> orig = new ArrayList<String>(Arrays.asList(original));
	final static List<String> exp = new ArrayList<String>(Arrays.asList(expected));

	@Test
	public void test() {
		Iterator<String> origin = orig.iterator();
		Iterator<String> expect = exp.iterator();
		Polynom polynom = new Polynom("");
		while (origin.hasNext() && expect.hasNext()) {
			polynom.setPoly(origin.next());
			assertEquals(polynom.getPoly(), expect.next());
		}
	}

}
