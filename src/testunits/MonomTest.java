package testunits;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import backend.Monom;

public class MonomTest {

	@Test
	public void test() {

		Monom test = new Monom();

		test.setMonom("x");
		assertEquals("+x", test.getMonom());
		test.setMonom("x^0");
		assertEquals("+1", test.getMonom());
		test.setMonom("1");
		assertEquals("+1", test.getMonom());
		test.setMonom("2");
		assertEquals("+2", test.getMonom());
		test.setMonom("2x");
		assertEquals("+2x", test.getMonom());
		test.setMonom("x^2");
		assertEquals("+x^2", test.getMonom());
		test.setMonom("1x");
		assertEquals("+x", test.getMonom());
		test.setMonom("x^1");
		assertEquals("+x", test.getMonom());
		test.setMonom("3x^1");
		assertEquals("+3x", test.getMonom());
		test.setMonom("3x^4");
		assertEquals("+3x^4", test.getMonom()); // only valid formats are tested because the user cannot create a monom
		// a user creates a polynom and that'll validate each monom and then if a monom
		// is valid it'll be created otherwise not

	}

}
