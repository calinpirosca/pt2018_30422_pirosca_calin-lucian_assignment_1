package testunits;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FunctionsTest.class, MonomTest.class, PolynomTest.class })
public class AllTests {

}
