package testunits;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import backend.Functions;
import backend.Polynom;

public class FunctionsTest {

	@Test
	public void test() {
		
		int i = 0;
		int j = 0;
		List<Polynom> testbase = new ArrayList<Polynom>() {{
			add(new Polynom("1"));add(new Polynom("-3"));add(new Polynom("-x"));add(new Polynom("+3x"));add(new Polynom("x^1"));
			add(new Polynom("+3x^1"));add(new Polynom("-2x^5"));add(new Polynom("2x^5-2x^5+3/4x+2x^1+2"));
			add(new Polynom("1/4x"));
		}};
		List<String> expectedAdd = new ArrayList<String>() {{
			add("-2");add("-x+1");add("3x+1");add("x+1");add("3x+1");add("-2x^5+1");add("11/4x+3");add("1/4x+1");
			add("-x-3");add("3x-3");add("x-3");add("3x-3");add("-2x^5-3");add("11/4x-1");add("1/4x-3");
			add("2x");add("0");add("2x");add("-2x^5-x");add("7/4x+2");add("-3/4x");
			add("4x");add("6x");add("-2x^5+3x");add("23/4x+2");add("13/4x");
			add("4x");add("-2x^5+x");add("15/4x+2");add("5/4x");
			add("-2x^5+3x");add("23/4x+2");add("13/4x");
			add("-2x^5+11/4x+2");add("-2x^5+1/4x");
			add("3x+2");
		}};
		List<String> expectedDifference = new ArrayList<String>() {{
			add("4");add("x+1");add("-3x+1");add("-x+1");add("-3x+1");add("2x^5+1");add("-11/4x-1");add("-1/4x+1");
			add("x-3");add("-3x-3");add("-x-3");add("-3x-3");add("2x^5-3");add("-11/4x-5");add("-1/4x-3");
			add("-4x");add("-2x");add("-4x");add("2x^5-x");add("-15/4x-2");add("-5/4x");
			add("2x");add("0");add("2x^5+3x");add("1/4x-2");add("11/4x");
			add("-2x");add("2x^5+x");add("-7/4x-2");add("3/4x");
			add("2x^5+3x");add("1/4x-2");add("11/4x");
			add("-2x^5-11/4x-2");add("-2x^5-1/4x");
			add("5/2x+2");
		}};
		List<Polynom> testbasemul = new ArrayList<Polynom>() {{
			add(new Polynom("1x+x^2"));add(new Polynom("-3x"));add(new Polynom("-x"));
		}};
		List<String> expectedMultiplication = new ArrayList<String>() {{
			add("-3x^3-3x^2");add("-x^3-x^2");add("3x^2");
		}};
		
		
		for(Polynom x : testbase) {
			for(Polynom y : testbase.subList(i+1, testbase.size())) {
				String tempAdd = Functions.add(x, y, false).getPoly();
				String tempDif = Functions.substract(x, y).getPoly();
				String temp1Add = expectedAdd.get(i+j);
				String temp1Dif = expectedDifference.get(i+j);
				assertEquals(temp1Add, tempAdd);
				assertEquals(temp1Dif, tempDif);
				j++;
			}
			i++;
			j--;
		}
		i = 0;
		j = 0;
		for(Polynom x : testbasemul) {
			for(Polynom y : testbasemul.subList(i+1, testbasemul.size())) {
				String tempMul = Functions.multiply(x, y).getPoly();
				String temp1Mul = expectedMultiplication.get(i+j);
				assertEquals(temp1Mul, tempMul);
				j++;
			}
			i++;
			j--;
		}
		assertEquals("-2/3x-5/9", Functions.divide(new Polynom("1x+2x^2"), new Polynom("-3x+1")).get(0).getPoly()); // test the quotient
		assertEquals("5/9", Functions.divide(new Polynom("1x+2x^2"), new Polynom("-3x+1")).get(1).getPoly()); //test the remainder
		assertEquals("2/3x^3+1/2x^2", Functions.integrate(new Polynom("1x+2x^2")).getPoly());
		assertEquals("4x+1", Functions.derivation(new Polynom("1x+2x^2")).getPoly());
	}
}
